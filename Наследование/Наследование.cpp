﻿#include <iostream>
#include<String>

using namespace std;

class Animals
{
public:
    std::string AnimalSays;
    Animals(std::string _AnimalSays = "Error") : AnimalSays(_AnimalSays) {}
    void Voice()
    {
        std::cout << AnimalSays << "\n";
    }

};

class Dog : public Animals
{
public:
    Dog(std::string _AnimalSays = "Woof!") : Animals(_AnimalSays)
    {}

};

class Cat : public Animals
{
    public:
    Cat(std::string _AnimalSays = "Meow!") : Animals(_AnimalSays)
    {}
};

class Dinosaur : public Animals 
{
public:
    Dinosaur(std::string _AnimalSays = "ROAR!") : Animals(_AnimalSays)
    {}

};
int main()
{
    Dinosaur temp;
    Animals* obj[3];
    obj[0] = new Dinosaur();
    obj[1] = new Dog();
    obj[2] = new Cat();
    for (int i = 0; i < 3; i++)
    {
        
        obj[i]->Voice();

    }
    delete obj[0];
    delete obj[1];
    delete obj[2];
   
}
